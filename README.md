# Bitwijz

A simple, free/libre and open-source memorization game for converting
hexadecimal numbers to their binary equivalents as fast as possible.

Made using [Godot](https://godotengine.org), [LMMS](https://lmms.io)
and [LADSPA](https://www.ladspa.org/).

Best played with audio.

## Goals

This project was put together in less than a day to help myself and others
memorize hexadecimal numbers fast. It's meant to be fun, but stressful, to
maximize productivity and simulate the anxiety of an examination environment.

Overall adherence to OSS standards is also a goal.

## Usage

This project can be directly opened in the
[Godot Editor](https://editor.godotengine.org/releases/latest/)
and exported for all desired platforms.

You can also use the command line directly to export the project, provided that
you have Godot installed.

The following example illustrates how to export the game to HTML5/WebAssembly:

```
godot --path . --export HTML5
```

## License

All code is licensed under the [BSD 3-clause](LICENSE.md) License.

All music and sound effects in this repository (All files ending in `.ogg`,
`.wav`, `.flac`, `.mp3` and `.mmpz`) are licensed under the
[CC BY-NC 4.0](https://creativecommons.org/licenses/by-nc/4.0/legalcode) License.
[LICENSE-ART.md](LICENSE-ART.md) analyze the distribution terms for each file
in greater detail.
