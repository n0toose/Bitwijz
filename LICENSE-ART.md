# Licensing terms for Bitwijz assets

## Music

### Bitwijz (`bitwijz.ogg`)

Copyright by **Panagiotis "Ivory" Vasilopoulos (n0toose)**

License: [CC BY-NC 4.0](https://creativecommons.org/licenses/by-nc/4.0/)

### Instructions (`instructions.ogg`)

Copyright by **Panagiotis "Ivory" Vasilopoulos (n0toose)**

License: [CC BY-NC 4.0](https://creativecommons.org/licenses/by-nc/4.0/)

### Main Menu (`menu.ogg`)

Copyright by **Panagiotis "Ivory" Vasilopoulos (n0toose)**

License: [CC BY-NC 4.0](https://creativecommons.org/licenses/by-nc/4.0/)

## Sound Effects

### `true_sound.ogg`

Copyright by **Panagiotis "Ivory" Vasilopoulos (n0toose)**

License: [CC BY-NC 4.0](https://creativecommons.org/licenses/by-nc/4.0/)

### `false_sound.ogg`

Copyright by **Panagiotis "Ivory" Vasilopoulos (n0toose)**

License: [CC BY-NC 4.0](https://creativecommons.org/licenses/by-nc/4.0/)

### `zero_entered.ogg`

Copyright by **Panagiotis "Ivory" Vasilopoulos (n0toose)**

License: [CC BY-NC 4.0](https://creativecommons.org/licenses/by-nc/4.0/)

### `one_entered.ogg`

Copyright by **Panagiotis "Ivory" Vasilopoulos (n0toose)**

License: [CC BY-NC 4.0](https://creativecommons.org/licenses/by-nc/4.0/)

---

The used license also extends to the project files of each file, which are
shipped with this repository to allow for easier modification in the future,
as well as archival reasons.

## Fonts

### Xolonium

Copyright by **Severin Meyer**

License: [SIL Open Font License Version 1.1](assets/fonts/xolonium/xolonium-fonts-4.1/LICENSE.txt)
