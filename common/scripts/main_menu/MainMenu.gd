extends MarginContainer

onready var selectionCounter = 0
onready var selections = [
	$CenterContainer/VBoxContainer/Settings/CenterContainer/VBoxContainer/StartContainer/SelectionMarker,
	$CenterContainer/VBoxContainer/Settings/CenterContainer/VBoxContainer/QuitContainer/SelectionMarker
]

onready var scenes = [
	"res://common/scenes/instructions/Instructions.tscn",
	# We use a scene for quitting, as that allows us to be more flexible with
	# adding additional scenes.
	"res://common/scenes/quit/Quit.tscn"
]

func set_current_selection(_selection):
	for item in selections:
		item.text = ""

	selections[_selection].text = "·"

func handle_selection(_selection) -> void:
	get_tree().change_scene(scenes[_selection])

func _ready() -> void:
	set_current_selection(selectionCounter)

func _process(_delta) -> void:
	if Input.is_action_just_pressed("ui_down"):
		selectionCounter += 1
		set_current_selection(selectionCounter % selections.size())
	if Input.is_action_just_pressed("ui_up"):
		selectionCounter -= 1
		set_current_selection(selectionCounter % selections.size())
	if Input.is_action_just_pressed("ui_accept"):
		handle_selection(abs(selectionCounter % selections.size()))
