extends Node2D

signal timeout

onready var totalTime: float = 60.9
onready var remainingTime: float = totalTime
onready var updateTime : float = 1.0
onready var lastSuccessTime : float = totalTime
onready var selectionCounter = 0
onready var sceneTimer = $Timer
onready var randomizer = RandomNumberGenerator.new()

onready var randomPosition = 0
onready var binarySolution = "0000"
onready var hexSolution = "0"
onready var numberArray = ""
onready var score = 0

onready var onscreenTimeLeft = $CenterContainer/VBoxContainer/Title/HBoxContainer/timeLeft
onready var onscreenScore = $CenterContainer/VBoxContainer/VBoxContainer/HBoxContainer/ScoreNum
onready var onscreenHex = $CenterContainer/VBoxContainer/HexView/CenterContainer/Hex
onready var onscreenNumbers = [
	$CenterContainer/VBoxContainer/InputField/HBoxContainer/Number1,
	$CenterContainer/VBoxContainer/InputField/HBoxContainer/Number2,
	$CenterContainer/VBoxContainer/InputField/HBoxContainer/Number3,
	$CenterContainer/VBoxContainer/InputField/HBoxContainer/Number4
]


onready var hexArray = [
	"0", "1", "2", "3", "4", "5", "6", "7",
	"8", "9", "A", "B", "C", "D", "E", "F"
]

onready var binArray = PoolStringArray([
	"0000", "0001", "0010", "0011", "0100", "0101", "0110", "0111",
	"1000", "1001", "1010", "1011", "1100", "1101", "1110", "1111"
])

func rightAnswer() -> void:
	calculateScore()
	numberArray = ""
	$TrueSound.play()
	setGeneratedNumber()

func wrongAnswer() -> void:
	selectionCounter = 0
	numberArray = ""
	$FalseSound.play()
	setGeneratedNumber()

func setGeneratedNumber() -> void:
	randomizer.randomize()
	var number = randomizer.randi_range(0, 15)
	binarySolution = binArray[number]
	hexSolution = hexArray[number]
	onscreenHex.text = hexSolution
	
	for object in onscreenNumbers:
		object.text = ""

func setChosenNumber(position, number) -> void:
	numberArray = numberArray + str(number)
	onscreenNumbers[position].text = str(number)
	
	if (number == 0):
		$ZeroEnteredSound.play()
	else:
		$OneEnteredSound.play()

func calculateScore() -> void:
	if ((lastSuccessTime - remainingTime) != 0):
		score += int((1 / (lastSuccessTime - remainingTime)) * 100)
	else:
		# Bonus reward for not causing my game to crash because of a
		# division by zero. Good player!
		score += 300

	onscreenScore.text = str(score)
	lastSuccessTime = remainingTime

func showResults() -> void:
	$FinalResultsBackground.show()
	
	# Show intimidating, red color if the score is too low.
	# Could be potentially interesting to calculate the color
	# algorithmically.
	if score < 150:
		$FinalResultsBackground.color = Color(1, 0.23, 0.2, 1)
	
	$ScoreContainer/FinalScoreNum.text = str(score)
	$ScoreContainer/FinalScoreNum.show()

func _on_Timer_timeout():
	# The time is always updated, even if it runs out.
	# This is useful for when the game is over, so that the player
	# can be returned to the main menu.
	remainingTime -= updateTime

	if remainingTime >= 0:
		onscreenTimeLeft.text = str(int(remainingTime))
	elif remainingTime < 56 and remainingTime >= -3:
		showResults()
	else:
		get_tree().change_scene("res://common/scenes/main_menu/MainMenu.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	lastSuccessTime = totalTime
	setGeneratedNumber()
	sceneTimer.set_wait_time(updateTime)
	sceneTimer.start()

func _process(_delta):
	if len(numberArray) == 4:
		if numberArray == binarySolution and remainingTime >= -1:
			rightAnswer()
		else:
			wrongAnswer()
	elif len(numberArray) >= 4: # Dirty bug fix when the player spams keys.
		wrongAnswer()

	if remainingTime >= 0:
		if Input.is_action_just_pressed("ui_down"):
			setChosenNumber(selectionCounter % 4, 0)
			selectionCounter += 1
		if Input.is_action_just_pressed("ui_up"):
			setChosenNumber(selectionCounter % 4, 1)
			selectionCounter += 1
