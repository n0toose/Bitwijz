extends MarginContainer

# onready var buttonLabel = $CenterContainer/VBoxContainer/VBoxContainer/HBoxContainer/Button;

var transitionAllowed = false
var frameCounter = 0;

func _ready() -> void:
	pass

func _process(_delta) -> void:
	# Leave some time for the player to read the instructions.
	# Also, since the same key is waited for in the previous scene, this
	# also gives them the chance to release the button so that they do not
	# get instantly skipped to the Game scene.
	if not transitionAllowed:
		frameCounter += 1;
		if frameCounter > 60: # approximately 0.0167 seconds with 60 FPS
			transitionAllowed = true
	
	if Input.is_action_pressed("ui_accept") and transitionAllowed:
		get_tree().change_scene("res://common/scenes/game/Game.tscn")
